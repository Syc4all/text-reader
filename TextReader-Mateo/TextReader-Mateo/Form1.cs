﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TextReader_Mateo
{
    public partial class Form1 : Form
    {
        List<string> list;
        string fileLocation = @"Locations.txt";

        public Form1()
        {
            list = new List<string>();
            InitializeComponent();
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = fileName.SelectedIndex;

            StreamReader reader = new StreamReader(list[index]);
            fileText.Text = reader.ReadToEnd();
            reader.Close();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            OpenFileDialog addFile = new OpenFileDialog();

            addFile.Filter = "TXT|*.txt";
            addFile.Title = " Add new text file";

            if(addFile.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            
            fileName.Items.Add(addFile.SafeFileName);

            StreamReader reader = new StreamReader(addFile.OpenFile());

            fileText.Text = reader.ReadToEnd();
            reader.Close();

            list.Add(addFile.FileName);

            StreamWriter writer = new StreamWriter(fileLocation,true);

            writer.WriteLine(addFile.FileName);
            writer.Close();
            

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if(!File.Exists(fileLocation))
            {

                FileStream fs = File.Create(fileLocation);
                fs.Close();
            }

            StreamReader reader = new StreamReader(fileLocation);
            
            while(!reader.EndOfStream)
            {
                list.Add(reader.ReadLine());
            }
            reader.Close();

            foreach(string location in list)
            {
                fileName.Items.Add(Path.GetFileName(location));
            }
           
        }

        private void Save_Click(object sender, EventArgs e)
        {
            int index = 0;

            index = fileName.SelectedIndex;

            StreamWriter writer = new StreamWriter(list[index]);

            writer.Write(fileText.Text);
            writer.Close();
        }

    }
}
